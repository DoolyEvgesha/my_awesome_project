//import java.util.logging.Logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorld {
    public static void main(String[] argv) {
        System.out.println("Hello world");
        Logger logger = LoggerFactory.getLogger(HelloWorld.class);
        logger.error("error");
    }
}
